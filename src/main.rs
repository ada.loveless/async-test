use std::{
    io::{stdout, Write},
    time::{Duration, Instant, SystemTime},
};

#[derive(Debug)]
struct Client {
    secret: String,
}

impl From<String> for Client {
    fn from(value: String) -> Self {
        Client { secret: value }
    }
}

impl Client {
    fn get(&self) -> State {
        std::thread::sleep(Duration::from_secs(2));
        State::now()
    }
}

#[derive(Debug)]
struct State {
    value: SystemTime,
}
impl State {
    fn now() -> Self {
        State {
            value: SystemTime::now(),
        }
    }
}

impl std::fmt::Display for State {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let result = SystemTime::now()
            .duration_since(self.value)
            .map(|x| format!("{:04}", x.as_millis()))
            .unwrap_or(String::from("invalid"));
        write!(f, "{}", result)
    }
}

fn main_loop(client: Client) {
    let mut state = client.get();
    let mut last_update = Instant::now();
    let sleep_duration = Duration::from_millis(10);
    let update_duration = Duration::from_secs(5);
    let mut stdout = stdout();

    loop {
        let now = Instant::now();
        if last_update + update_duration < now {
            log::debug!("Updating data");
            state = client.get();
            last_update = now;
            // update
        }
        print!("\r{state}");
        stdout.flush().unwrap();
        std::thread::sleep(sleep_duration)
    }
}

#[tokio::main]
async fn main() {
    env_logger::init();
    let client = Client::from(String::from("secret"));
    main_loop(client);
}
